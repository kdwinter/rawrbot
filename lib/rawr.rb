# The MIT License (MIT)
#
# Copyright (c) 2015 Gigamo <gigamo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require "socket"
require "active_support/ordered_options"

module IRC

  # Root directory
  ROOT = File.dirname(File.expand_path(File.join(__FILE__, "..")))

  class << self
    # Simple log functionality
    def L(message)
      time = Time.now
      time = time.strftime("%Y-%m-%dT%H:%M:%S.") << "%06d" % time.usec
      $stdout.puts("[#{time}] #{message}")
    end

    # Format an IRC Chat message
    def chat_message(recipient, message)
      return "PRIVMSG #{recipient} :#{message}"
    end

    # Format an IRC Chat action
    def chat_action(recipient, message)
      return chat_message(recipient, "\001ACTION #{message}\001")
    end

    # Shortcut
    def config
      Config.config(&Proc.new)
    end
    alias configure config
  end

end

# Even though plugins are reloadable on the fly, their dependencies do not need to be
def require_once(path, const)
  const     = const.split("::")
  top_const = const.shift()
  defined   = Object.const_defined?(top_const)

  const.each do |c|
    begin
      defined &&= Object.const_get(top_const).const_defined?(c)
      top_const = c
    rescue NameError
      defined = false
    end
  end

  if not defined
    IRC::L("Requiring #{path}")
    require path
  end
end

require_relative "rawr/version"
require_relative "rawr/config"
require_relative "rawr/plugin"
require_relative "rawr/plugins"
require_relative "rawr/sender"
require_relative "rawr/input_handler"
require_relative "rawr/bot"
