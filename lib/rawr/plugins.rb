# The MIT License (MIT)
#
# Copyright (c) 2015 Gigamo <gigamo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

module IRC

  module Plugins
    # Load or reload the plugin file for given plugin +name+
    def self.use(name)
      file = File.join(File.dirname(File.expand_path(__FILE__)), "plugins", "#{name.to_s}.rb")

      if File.exist?(file)
        if plugins.key?(name.to_sym)
          IRC::L("Reloading existing plugin: #{name}")
        end

        load(file)
      else
        IRC::L("Attempted to load plugin #{name}, but was not found")
      end
    end

    # Plugin map
    @_plugins = {}
    def self.plugins
      return @_plugins
    end

    # Find and return a plugin response for a given plugin +command+
    def self.resolve(command, sender, recipient, options = "")
      if plugin = plugins[command.to_sym]
        IRC::L("Matching response for #{command} was found")

        return plugin.new(sender, recipient).respond(options)
      else
        IRC::L("No response available for command: #{command}")
      end
    end

    # Register a new plugin which will be subclassed to +IRC::Plugin+.
    # Example:
    #
    #   IRC::Plugins.register(:command_1, :command_2) do
    #     def respond(options)
    #       return "response"
    #     end
    #   end
    def self.register(*commands, &block)
      commands.map!(&:to_sym)

      plugin = Class.new(IRC::Plugin, &block)
      plugin.name(commands.first)

      commands.each do |command|
        if plugins.key?(command)
          IRC::L("Overwriting existing plugin command: #{command}")
        end

        IRC::L("Registered plugin #{plugin.name} for command #{command}")

        plugins[command] = plugin
      end
    end
  end

end
