# The MIT License (MIT)
#
# Copyright (c) 2015 Gigamo <gigamo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

module IRC

  class Bot
    attr_accessor :registered
    attr_reader :nickname, :channels

    def initialize()
      @socket   = nil

      @server   = Config.server
      @port     = Config.port

      @nickname = Config.nickname
      @channels = [] # needs to be empty for join_channels call to work first time

      @registered = false

      @input = InputHandler.new(self)

      @hooks = EMPTY_HOOKS

      register_events()
    end

    # Register and listen for default events
    def register_events()
      on_event(:socket_opened) do
        send_message("SETNAME RawrBot")
        send_message("USER RawrBot * * :RawrBot")
        set_nickname(@nickname)
      end

      on_event(:connected) do
        if Config.password
          send_message(IRC::chat_message("NickServ", "IDENTIFY #{Config.password}"))
        end
        join_channels(Array(Config.channels))
      end

      on_event(:socket_closed) do
        leave_channels(@channels)
        send_message("QUIT")
      end
    end

    # Open a TCP connection to the configured server/port
    def connect()
      if @socket.nil?
        IRC::L("Attempting connection to #{@server}:#{@port}")

        @socket = TCPSocket.open(@server, @port)

        fire_event(:socket_opened)
      end
    end

    # Disconnect the TCP connection, if available
    def disconnect()
      if @socket
        fire_event(:socket_closed)

        @socket.flush()
        @socket.shutdown(2)
        @socket = nil
      end
    end

    # Send a message to the socket
    def send_message(message)
      @socket.send("#{message}\n", 2)

      IRC::L("-> #{message}")
    rescue => e
      IRC::L("Failed to send: #{message}")
    end

    # Main event loop; listens to both the socket and stdin for input
    #
    # In case of socket, input is parsed by an +IRC::InputHandler+
    def listen!()
      connect()

      loop do
        sources = IO.select([@socket, $stdin], nil, nil, nil)
        next unless sources

        sources[0].each do |source|
          return if source.eof

          if source == $stdin
            source = source.gets()
            send_message(source)
          elsif source == @socket
            source = source.gets()
            IRC::L("<- #{source}")
            @input.handle(source)
          end
        end
      end

    rescue Interrupt
      IRC::L("Interrupted")
    rescue Errno::ECONNREFUSED
      IRC::L("Can't connect to server")
    rescue Exception => e
      disconnect()

      IRC::L(e.message)
      IRC::L(e.backtrace.join("\n"))

      retry
    ensure
      disconnect()
    end


    # Set bot nickname
    def set_nickname(new_nickname)
      IRC::L("Setting nickname: #{new_nickname}")

      @nickname = new_nickname

      send_message("NICK #{@nickname}")
    end

    # Join irc channels
    def join_channels(new_channels = [])
      IRC::L("Joining channels: #{new_channels.join(", ")}")

      duplicates = @channels & new_channels
      new_channels -= duplicates

      fire_event(:joined_channels, new_channels)
      send_message("JOIN #{new_channels.join(?,)}")

      @channels += new_channels
    end

    # Leave irc channels
    def leave_channels(old_channels = [])
      IRC::L("Leaving channels: #{old_channels.join(", ")}")

      leavable_channels = @channels & old_channels

      fire_event(:left_channels, leavable_channels)
      send_message("PART #{leavable_channels.join(?,)}")

      @channels -= leavable_channels
    end


    # Basic event hook setup
    EMPTY_HOOKS = {
      :connected       => [],
      :socket_closed   => [],
      :joined_channels => [],
      :left_channels   => []
    }

    # Call and execute all registered hooks for a given event
    def fire_event(hook, *args)
      IRC::L("Fired hooks for ':#{hook}'")

      @hooks[hook].each { |block| block.call(*args) } if @hooks.key?(hook)
    end

    # Register a new hook for an event
    def on_event(hook, &block)
      IRC::L("Registered new hook for ':#{hook}'")

      @hooks[hook] ||= []
      @hooks[hook] << block
    end

    def registered?()
      return !!registered
    end
  end

end
