# The MIT License (MIT)
#
# Copyright (c) 2015 Gigamo <gigamo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

IRC::Plugins.register(:add, :add_command) do
  def self.responses
    # TODO: load these externally so they persist through sessions
    @responses ||= {}
    return @responses
  end

  def self.response(command, recipient)
    command = command.to_sym
    return IRC::chat_message(recipient, responses[command]) if responses.key?(command)
  end

  def respond(options)
    if admin?()
      cmd = options.shift()
      txt = options.join(' ')

      if !txt.nil? && !txt.empty?
        self.class.responses[cmd.to_sym] = txt

        return IRC::chat_message(recipient, "Registered command: #{cmd}")
      else
        return IRC::chat_message(recipient, "Command was not registered as no additional arguments were given: #{cmd}")
      end
    end
  end
end

