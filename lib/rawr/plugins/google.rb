# The MIT License (MIT)
#
# Copyright (c) 2015 Gigamo <gigamo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require_once "json", "JSON"
require_once "uri", "URI"
require_once "net/http", "Net::HTTP"

IRC::Plugins.register(:google, :search) do
  def respond(options)
    query = options.join(" ")

    if result = search(query)
      result = "#{query}: #{result}"
    else
      result = "No results found"
    end

    return IRC::chat_message(recipient, result)
  end

  def search(query)
    url  = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=#{URI.encode(query)}&rsz=large"
    data = Net::HTTP.get_response(URI.parse(url)).body

    begin
      return JSON.parse(data)['responseData']['results'].map { |x| x['unescapedUrl'] }[0]
    rescue => e
      return nil
    end
  end
end
