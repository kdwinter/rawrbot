# The MIT License (MIT)
#
# Copyright (c) 2015 Gigamo <gigamo@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

module IRC

  class InputHandler
    def initialize(bot)
      @bot = bot
    end

    # TODO: This needs to be cleaned up
    def handle(input)
      case input.strip()
      # Indicates the bot usermode has changed
      when /MODE\s#{@bot.nickname}\s:\+(.+)/
        mode = $1

        case mode
        when ?i # OFTC
          @bot.fire_event(:connected)
        when ?R # OFTC
          @bot.registered = true
        end

      # Server expects a PONG response
      when /\APING\s:(.+)\z/i
        server = $1

        @bot.send_message("PONG :#{server}")

      # Parse commands through the plugin system
      when /\A:(.+?)!(.+?)@(.+?)\sPRIVMSG\s(.+)\s:!(.+)\z/i,
           /\A:(.+?)!(.+?)@(.+?)\sPRIVMSG\s(.+)\s:#{@bot.nickname}:\s(.+)\z/i
        sender    = Sender.new($1, $2, $3)
        recipient = $4
        recipient = sender.nickname if recipient == @bot.nickname
        command, options = parse_options($5)

        response   = Plugins.resolve(command, sender, recipient, options)
        response ||= Plugins.plugins[:add].response(command, recipient) if Plugins.plugins.key?(:add)

        if response
          @bot.send_message(response)
        else
          @bot.send_message(IRC::chat_message(recipient, "I'm very sorry, #{sender.nickname}, but I don't recognize that command"))
        end

      # Not enough permissions?
      when /482\s#{@bot.nickname}\s(.+)\s:You're\snot\schannel\soperator\z/i
        channel = $1

        @bot.send_message(IRC::chat_message(channel, "I'd love to, but I don't have permission!"))

      end

    end

    private def parse_options(options)
      options = options.split()
      command = options.shift()

      return [command, options]
    end
  end

end
